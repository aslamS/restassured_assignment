package com.restassured;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class putmethod {
	@Test
	public void testPutOperation() {

		JSONObject req = new JSONObject();
		req.put("name", "asaf");
		req.put("email", "asfa@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");

		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer 34faa472deac9bc8a40e1228c5be1f06a45e6160b88db2d630d96e8a70e2b0f7")
				.body(req.toJSONString()).patch("/users/199419").then().statusCode(200);

	}

}
//199419