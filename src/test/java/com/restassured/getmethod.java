package com.restassured;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.response.Response;
public class getmethod {
	@Test
	public void testingGetOperation() {
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();
		Assert.assertEquals(200, statusCode);

	}
}
