package com.restassured;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import io.restassured.response.ValidatableResponse;

public class Postmethod {
	@Test
	public void testingPostOperation() {
		JSONObject req = new JSONObject();
		req.put("name", "Aslam");
		req.put("email", "aslam@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		ValidatableResponse Response = given().log().all().contentType("application/json")
				.header("authorization", "Bearer 34faa472deac9bc8a40e1228c5be1f06a45e6160b88db2d630d96e8a70e2b0f7")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);

	}

}